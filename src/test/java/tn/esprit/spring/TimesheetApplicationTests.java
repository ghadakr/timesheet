package tn.esprit.spring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Role;
import tn.esprit.spring.services.IEmployeService;

@SpringBootTest
class TimesheetApplicationTests {

	@Autowired
	IEmployeService employeService;

	Employe employe = new Employe("test-junit-1", "test-junit-1", "test-junit", "test-junit", true, Role.TECHNICIEN);

	private static final Logger l = Logger.getLogger(TimesheetApplicationTests.class);

	@Test
	void addEmployeTest() {

		int employeNumberBeforTest = employeService.getNombreEmployeJPQL();

		l.info(" \n *************************** le nombre des employes avant l'ajout " + employeNumberBeforTest +"*************************  \n" );

		employeService.addOrUpdateEmploye(this.employe);
		
		int employeNumberAfterTest = employeService.getNombreEmployeJPQL();

		l.info(" \n *************************** le nombre des employes aprés l'ajout " + employeNumberAfterTest +"*************************  \n" );
		
	

		assertEquals(employeNumberBeforTest, employeNumberAfterTest - 1);
	}

}
